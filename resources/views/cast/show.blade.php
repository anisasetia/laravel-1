@extends('layout.master')

@section('judul')
Halaman Detail Cast {{$cast->nama}}
@endsection

@section('content') 

<h3>{{$cast->nama}}</h3>
<h4>{{$cast->umur}}</h4>
<p>{{$cast->bio}}</p>

@endsection 
