@extends('layout.master')

@section('judul')
Media Online
@endsection

@section('content')
<h3>Sosial Media Developer</h3>
<p>Belajar dan berbagi agar hidup menjadi lebih baik</p>   

<h3> Join di Media Sosial</h3>
<ul>
    <li>Mendapatkan motivasi dari sesama para Developer</li>
    <li>Sharing Knowledge</li>
    <li>Dibuat oleh calon web developer terbaik</li>
  </ul> 

<h3>Cara Bergabung ke Media Online</h3>
<ol>
  <li>Mengunjungi Website ini</li>
  <li>Mendaftarkan di <a href="/form">Form Sign Up</a></li>
  <li>Selesai</li>
</ol> 
@endsection
